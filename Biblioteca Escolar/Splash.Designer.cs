﻿namespace Biblioteca_Escolar
{
    partial class Splash
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Splash));
            this.timerSplash = new System.Windows.Forms.Timer(this.components);
            this.LblCargando = new System.Windows.Forms.Label();
            this.pgbCargando = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // timerSplash
            // 
            this.timerSplash.Interval = 50;
            this.timerSplash.Tick += new System.EventHandler(this.timerSplash_Tick);
            // 
            // LblCargando
            // 
            this.LblCargando.BackColor = System.Drawing.Color.Transparent;
            this.LblCargando.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCargando.Location = new System.Drawing.Point(224, 21);
            this.LblCargando.Name = "LblCargando";
            this.LblCargando.Size = new System.Drawing.Size(344, 36);
            this.LblCargando.TabIndex = 1;
            this.LblCargando.Text = "Cargando....";
            // 
            // pgbCargando
            // 
            this.pgbCargando.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pgbCargando.Location = new System.Drawing.Point(293, 34);
            this.pgbCargando.Name = "pgbCargando";
            this.pgbCargando.Size = new System.Drawing.Size(234, 23);
            this.pgbCargando.TabIndex = 0;
            // 
            // Splash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(645, 365);
            this.Controls.Add(this.LblCargando);
            this.Controls.Add(this.pgbCargando);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Splash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cargando....";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timerSplash;
        private System.Windows.Forms.Label LblCargando;
        private System.Windows.Forms.ProgressBar pgbCargando;
    }
}


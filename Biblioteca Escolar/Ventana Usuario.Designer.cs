﻿namespace Biblioteca_Escolar
{
    partial class Ventana_Usuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // btnAdministrarUsuarios
            // 
            this.btnAdministrarUsuarios.Enabled = false;
            // 
            // btnConsultarLibros
            // 
            this.btnConsultarLibros.Click += new System.EventHandler(this.btnConsultarLibros_Click);
            // 
            // btnAdministrarLibros
            // 
            this.btnAdministrarLibros.Enabled = false;
            // 
            // btnConsultarUsuarios
            // 
            this.btnConsultarUsuarios.Enabled = false;
            this.btnConsultarUsuarios.Click += new System.EventHandler(this.btnConsultarUsuarios_Click);
            // 
            // btnPrestamoLibro
            // 
            this.btnPrestamoLibro.Enabled = false;
            this.btnPrestamoLibro.Click += new System.EventHandler(this.btnPrestamoLibro_Click);
            // 
            // btnPortadaLibro
            // 
            this.btnPortadaLibro.Click += new System.EventHandler(this.btnPortadaLibro_Click);
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(354, 34);
            this.lblNombre.Size = new System.Drawing.Size(71, 21);
            this.lblNombre.Text = "Usuario";
            // 
            // btnCerrarSesion
            // 
          
            // Ventana_Usuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(800, 543);
            this.Name = "Ventana_Usuario";
            this.Text = "Ventana_Usuario";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Ventana_Usuario_FormClosing);
            this.Load += new System.EventHandler(this.Ventana_Usuario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
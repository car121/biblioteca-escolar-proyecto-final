﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Conexion;

namespace Biblioteca_Escolar
{
    public partial class VentanaPrestamo : Form
    {
        public VentanaPrestamo()
        {
            InitializeComponent();
        }
        public DataSet LlenarDataGV(string tabla)
        {
            DataSet DS;
            string cmd = string.Format("SELECT * FROM " + tabla);
            DS = Utilidades.Ejecutar(cmd);

            return DS;
        }
        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar_Libros ConsultaLibros = new Consultar_Libros();
            ConsultaLibros.ShowDialog();


            if (ConsultaLibros.DialogResult == DialogResult.OK)
            {
                txtNombreLibro.Text = ConsultaLibros.dtgvLibros.Rows[ConsultaLibros.dtgvLibros.CurrentRow.Index].Cells[0].Value.ToString();
                txtIdLibro.Text = ConsultaLibros.dtgvLibros.Rows[ConsultaLibros.dtgvLibros.CurrentRow.Index].Cells[7].Value.ToString();

            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            Consultar_Usuarios ConsultaUsuarios = new Consultar_Usuarios();

            ConsultaUsuarios.ShowDialog();




            if (ConsultaUsuarios.DialogResult == DialogResult.OK)
            {
                txtNombreUsuario.Text = ConsultaUsuarios.dtgvLibros.Rows[ConsultaUsuarios.dtgvLibros.CurrentRow.Index].Cells[0].Value.ToString();
                txtIdUsuario.Text = ConsultaUsuarios.dtgvLibros.Rows[ConsultaUsuarios.dtgvLibros.CurrentRow.Index].Cells[7].Value.ToString();
            }
        }

        private void VentanaPrestamo_Load(object sender, EventArgs e)
        {
            dtgvPrestamo.DataSource = LlenarDataGV("Prestamos").Tables[0];
            txtPrestamo.Text = "0";
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {

                string cmd = string.Format("EXEC ActualizaPrestamo '{0}','{1}','{2}','{3}','{4}','{5}'",txtIdUsuario.Text.Trim(),txtIdLibro.Text.Trim(),dtpFecha.Value.ToString().Trim(),txtNombreLibro.Text.Trim(),txtNombreUsuario.Text.Trim(),txtPrestamo.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("SE HA GUARDADO CORRECTAMENTE");

                txtPrestamo.Text = "0";
                txtNombreLibro.Text = "";
                txtNombreUsuario.Text = "";
                txtIdLibro.Text = "";
                txtIdUsuario.Text = "";

            }
            catch (Exception error)
            {

                MessageBox.Show("Ha Ocurrido un error " + error.Message);

            }
            dtgvPrestamo.DataSource = LlenarDataGV("Prestamos").Tables[0];
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            txtPrestamo.Text = dtgvPrestamo.Rows[dtgvPrestamo.CurrentRow.Index].Cells[1].Value.ToString();
            string cmd = string.Format("EXEC EliminaPrestamo '{0}'", txtPrestamo.Text.Trim());
            Utilidades.Ejecutar(cmd);
            MessageBox.Show("SE HA ELIMINADO CORRECTAMENTE");
            dtgvPrestamo.DataSource = LlenarDataGV("Prestamos").Tables[0];

            txtPrestamo.Text = "";
           

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca_Escolar
{
    public partial class Splash : Form
    {
        public Splash()
        {
            InitializeComponent();
        }

        private void timerSplash_Tick(object sender, EventArgs e)
        {
            pgbCargando.Increment(1);

            if (pgbCargando.Value % 3 == 0)
            {

                LblCargando.Text += ".";

            }
            else
            {

                if(pgbCargando.Value%9==0){

                    LblCargando.Text = "Cargando.";
                }
            }


            if (pgbCargando.Value == 100)
            {

                timerSplash.Stop();
            }


        }
    }
}

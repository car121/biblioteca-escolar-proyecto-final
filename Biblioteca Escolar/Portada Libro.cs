﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Conexion;

namespace Biblioteca_Escolar
{
    public partial class Sinopsis_Libro : Form
    {
        public Sinopsis_Libro()
        {
            InitializeComponent();
        }
        public DataSet LlenarComboBox(string Tabla)
        {
            DataSet DS;
            string cmd = string.Format("SELECT Nombre FROM " + Tabla);
            DS = Utilidades.Ejecutar(cmd);

            return DS;
        }

        private void Sinopsis_Libro_Load(object sender, EventArgs e)
        {
            cmbLibros.DataSource = LlenarComboBox("Libro").Tables[0];
            cmbLibros.DisplayMember = "Nombre";
        }

        private void btnElegirLibro_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet DS;
                string cmd = string.Format("SELECT * FROM Libro WHERE Nombre ='{0}'", cmbLibros.Text);
                DS = Utilidades.Ejecutar(cmd);

                

                string imagen = DS.Tables[0].Rows[0]["Img"].ToString().Trim();

                pcbImagen.ImageLocation = imagen;
            }
            catch(Exception error)
            {
                MessageBox.Show(error.Message);

            }
        }
    }
}

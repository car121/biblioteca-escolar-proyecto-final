﻿namespace Biblioteca_Escolar
{
    partial class Ventana_Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblConsultarLibros = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnConsultarUsuarios = new System.Windows.Forms.Button();
            this.btnAdministrarLibros = new System.Windows.Forms.Button();
            this.btnConsultarLibros = new System.Windows.Forms.Button();
            this.btnAdministrarUsuarios = new System.Windows.Forms.Button();
            this.btnPrestamoLibro = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPortadaLibro = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(64, 234);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 20);
            this.label1.TabIndex = 1;
            // 
            // lblConsultarLibros
            // 
            this.lblConsultarLibros.AutoSize = true;
            this.lblConsultarLibros.BackColor = System.Drawing.Color.Silver;
            this.lblConsultarLibros.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultarLibros.Location = new System.Drawing.Point(328, 420);
            this.lblConsultarLibros.Name = "lblConsultarLibros";
            this.lblConsultarLibros.Size = new System.Drawing.Size(140, 20);
            this.lblConsultarLibros.TabIndex = 3;
            this.lblConsultarLibros.Text = "Consultar Libros";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(64, 420);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Administrar Libros";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(319, 234);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Consultar Usuarios";
            // 
            // btnConsultarUsuarios
            // 
            this.btnConsultarUsuarios.BackgroundImage = global::Biblioteca_Escolar.Properties.Resources.Consultar_Usuarios;
            this.btnConsultarUsuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConsultarUsuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultarUsuarios.ForeColor = System.Drawing.Color.DarkRed;
            this.btnConsultarUsuarios.Location = new System.Drawing.Point(312, 96);
            this.btnConsultarUsuarios.Name = "btnConsultarUsuarios";
            this.btnConsultarUsuarios.Size = new System.Drawing.Size(179, 158);
            this.btnConsultarUsuarios.TabIndex = 6;
            this.btnConsultarUsuarios.UseVisualStyleBackColor = true;
            this.btnConsultarUsuarios.Click += new System.EventHandler(this.btnConsultarUsuarios_Click);
            // 
            // btnAdministrarLibros
            // 
            this.btnAdministrarLibros.BackgroundImage = global::Biblioteca_Escolar.Properties.Resources.Administrar_Libros;
            this.btnAdministrarLibros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAdministrarLibros.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdministrarLibros.ForeColor = System.Drawing.Color.DarkRed;
            this.btnAdministrarLibros.Location = new System.Drawing.Point(61, 282);
            this.btnAdministrarLibros.Name = "btnAdministrarLibros";
            this.btnAdministrarLibros.Size = new System.Drawing.Size(179, 158);
            this.btnAdministrarLibros.TabIndex = 4;
            this.btnAdministrarLibros.UseVisualStyleBackColor = true;
            this.btnAdministrarLibros.Click += new System.EventHandler(this.btnAdministrarLibros_Click);
            // 
            // btnConsultarLibros
            // 
            this.btnConsultarLibros.BackgroundImage = global::Biblioteca_Escolar.Properties.Resources.Consultar_Libros;
            this.btnConsultarLibros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConsultarLibros.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultarLibros.ForeColor = System.Drawing.Color.DarkRed;
            this.btnConsultarLibros.Location = new System.Drawing.Point(312, 282);
            this.btnConsultarLibros.Name = "btnConsultarLibros";
            this.btnConsultarLibros.Size = new System.Drawing.Size(183, 158);
            this.btnConsultarLibros.TabIndex = 2;
            this.btnConsultarLibros.UseVisualStyleBackColor = true;
            this.btnConsultarLibros.Click += new System.EventHandler(this.btnConsultarLibros_Click);
            // 
            // btnAdministrarUsuarios
            // 
            this.btnAdministrarUsuarios.BackgroundImage = global::Biblioteca_Escolar.Properties.Resources.Administrar_Usuario;
            this.btnAdministrarUsuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAdministrarUsuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdministrarUsuarios.ForeColor = System.Drawing.Color.DarkRed;
            this.btnAdministrarUsuarios.Location = new System.Drawing.Point(61, 96);
            this.btnAdministrarUsuarios.Name = "btnAdministrarUsuarios";
            this.btnAdministrarUsuarios.Size = new System.Drawing.Size(179, 158);
            this.btnAdministrarUsuarios.TabIndex = 0;
            this.btnAdministrarUsuarios.UseVisualStyleBackColor = true;
            this.btnAdministrarUsuarios.Click += new System.EventHandler(this.btnAdministrarUsuarios_Click);
            // 
            // btnPrestamoLibro
            // 
            this.btnPrestamoLibro.BackgroundImage = global::Biblioteca_Escolar.Properties.Resources.pdomicilio;
            this.btnPrestamoLibro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPrestamoLibro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrestamoLibro.ForeColor = System.Drawing.Color.DarkRed;
            this.btnPrestamoLibro.Location = new System.Drawing.Point(563, 96);
            this.btnPrestamoLibro.Name = "btnPrestamoLibro";
            this.btnPrestamoLibro.Size = new System.Drawing.Size(179, 158);
            this.btnPrestamoLibro.TabIndex = 8;
            this.btnPrestamoLibro.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(585, 234);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Prestamo Libro\r\n";
            // 
            // btnPortadaLibro
            // 
            this.btnPortadaLibro.BackgroundImage = global::Biblioteca_Escolar.Properties.Resources.Portada;
            this.btnPortadaLibro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPortadaLibro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPortadaLibro.ForeColor = System.Drawing.Color.DarkRed;
            this.btnPortadaLibro.Location = new System.Drawing.Point(559, 282);
            this.btnPortadaLibro.Name = "btnPortadaLibro";
            this.btnPortadaLibro.Size = new System.Drawing.Size(183, 158);
            this.btnPortadaLibro.TabIndex = 10;
            this.btnPortadaLibro.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(575, 420);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Portadas Libros";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.BackColor = System.Drawing.Color.Red;
            this.btnCerrarSesion.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCerrarSesion.Location = new System.Drawing.Point(285, 472);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(242, 28);
            this.btnCerrarSesion.TabIndex = 12;
            this.btnCerrarSesion.Text = "Cerrar Sesion";
            this.btnCerrarSesion.UseVisualStyleBackColor = false;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(64, 234);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Administrar Usuarios";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(271, 34);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(70, 21);
            this.lblNombre.TabIndex = 14;
            this.lblNombre.Text = "Nombre";
            // 
            // Ventana_Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSalmon;
            this.ClientSize = new System.Drawing.Size(860, 512);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnCerrarSesion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnPortadaLibro);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnPrestamoLibro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnConsultarUsuarios);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAdministrarLibros);
            this.Controls.Add(this.lblConsultarLibros);
            this.Controls.Add(this.btnConsultarLibros);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAdministrarUsuarios);
            this.Name = "Ventana_Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ventana_Principal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Ventana_Principal_FormClosing);
            this.Load += new System.EventHandler(this.Ventana_Principal_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnAdministrarUsuarios;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnConsultarLibros;
        private System.Windows.Forms.Label lblConsultarLibros;
        public System.Windows.Forms.Button btnAdministrarLibros;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button btnConsultarUsuarios;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Button btnPrestamoLibro;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Button btnPortadaLibro;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Button btnCerrarSesion;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Conexion;
namespace Biblioteca_Escolar
{
    public partial class Ventana_Administrador : Ventana_Principal
    {
        

        public Ventana_Administrador()
        {
            InitializeComponent();
        }

        private void btnConsultarUsuarios_Click(object sender, EventArgs e)
        {
            Consultar_Usuarios ConsultaUsuarios = new Consultar_Usuarios();
            ConsultaUsuarios.ShowDialog();

            

        }

        private void btnConsultarLibros_Click(object sender, EventArgs e)
        {
            Consultar_Libros ConsultarLibros = new Consultar_Libros();

            ConsultarLibros.ShowDialog();
            
           


        }

        private void btnAdministrarUsuarios_Click(object sender, EventArgs e)
        {
            Administrar_Usuario AdministraUsuario = new Administrar_Usuario();
            AdministraUsuario.Show();
            this.Hide();
      
         
        }

        private void btnAdministrarLibros_Click(object sender, EventArgs e)
        {

            Administrar_Libros AdministraLibros = new Administrar_Libros();

            AdministraLibros.Show();

            this.Hide();
         
        }

        private void Ventana_Administrador_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void Ventana_Administrador_Load(object sender, EventArgs e)
        {
            


        }

        private void btnPrestamoLibro_Click(object sender, EventArgs e)
        {
            VentanaPrestamo ventanaPrestamo = new VentanaPrestamo();
            ventanaPrestamo.Show();

        }

        private void btnSinopsisLibro_Click(object sender, EventArgs e)
        {
          

        }

        private void btnPortadaLibro_Click(object sender, EventArgs e)
        {
            Sinopsis_Libro PortadaLibro = new Sinopsis_Libro();
            PortadaLibro.Show();
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
          
        }
    }
}

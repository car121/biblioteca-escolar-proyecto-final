﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Conexion;

namespace Biblioteca_Escolar
{
    public partial class Consultas : Form
    {
        public Consultas()
        {
            InitializeComponent();
        }


        public DataSet LlenarDataGV(string tabla)
        {
            DataSet DS;
            string cmd = string.Format("SELECT * FROM " + tabla);
            DS = Utilidades.Ejecutar(cmd);

            return DS;
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            if (dtgvLibros.Rows.Count == 0)
            {
                return;

            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }


        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
           
        }

        private void Consultas_Load(object sender, EventArgs e)
        {

        }
    }
}

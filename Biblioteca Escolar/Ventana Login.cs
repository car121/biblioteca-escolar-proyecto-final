﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;
using Conexion;

namespace Biblioteca_Escolar
{
    public partial class Ventana_Login : Form
    {
      

        public Ventana_Login()
        {

     

            Thread t = new Thread(new ThreadStart(SplashStart));

            t.Start();

            Thread.Sleep(5000);

            InitializeComponent();

            t.Abort();
        }

        private void Ventana_Login_Load(object sender, EventArgs e)

        { 
           


        }

        public void SplashStart()
        {

            Application.Run(new Splash());
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                string CMD = string.Format("Select * From Usuarios Where NomUsu = '{0}' AND Contrasena='{1}'", txtUsuario.Text.Trim(), txtContraseña.Text.Trim());

                DataSet ds = Utilidades.Ejecutar(CMD);

                string Cuenta = ds.Tables[0].Rows[0]["NomUsu"].ToString().Trim();
                string Contra = ds.Tables[0].Rows[0]["Contrasena"].ToString().Trim();
                string Rol = ds.Tables[0].Rows[0]["Administrador"].ToString().Trim();

                if (Cuenta == txtUsuario.Text.Trim() && Contra == txtContraseña.Text.Trim() && Rol == "True")
                {

                    Ventana_Administrador VAdmin = new Ventana_Administrador();

                    VAdmin.Show();

                    this.Hide();

                }
                
                if(Cuenta == txtUsuario.Text.Trim() && Contra == txtContraseña.Text.Trim() && Rol == "False")
                {

                    Ventana_Usuario VUsuario = new Ventana_Usuario();

                    VUsuario.Show();

                    this.Hide();

                }




            } catch(Exception error){




                MessageBox.Show("Ha Ocurrido un error" + error.Message,"ERROR",MessageBoxButtons.OK,MessageBoxIcon.Warning);




            }



        }

        private void Ventana_Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

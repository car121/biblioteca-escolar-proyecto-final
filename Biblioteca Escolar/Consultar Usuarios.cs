﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Conexion;


namespace Biblioteca_Escolar
{
    public partial class Consultar_Usuarios : Consultas
    {

     

        public Consultar_Usuarios()
        {
            InitializeComponent();
        }

        private void Consultar_Usuarios_Load(object sender, EventArgs e)
        {
            dtgvLibros.DataSource = LlenarDataGV("Usuarios").Tables[0];
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtBuscar.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;
                    string cmd = "Select * FROM Usuarios WHERE Nombre LIKE ('%" + txtBuscar.Text.Trim() + "%') ";

                    ds = Utilidades.Ejecutar(cmd);
                    dtgvLibros.DataSource = ds.Tables[0];
                }
                catch(Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error" + error.Message);
                }

            }
            
        }

        private void Consultar_Usuarios_FormClosing(object sender, FormClosingEventArgs e)
        {


           

          


         

            

        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {


     string  id = dtgvLibros.CurrentCell.Value.ToString();

            label3.Text = id;

            Administrar_Usuario AdUsuario = new Administrar_Usuario(id);
            
            
            
        }
    }
}

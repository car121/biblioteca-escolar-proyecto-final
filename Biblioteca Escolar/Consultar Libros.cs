﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Conexion;

namespace Biblioteca_Escolar
{
    public partial class Consultar_Libros : Consultas
    {
        public Consultar_Libros()
        {
            InitializeComponent();
        }


       
        private void Consultar_Libros_Load(object sender, EventArgs e)
        {
            dtgvLibros.DataSource = LlenarDataGV("Libro").Tables[0];

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtBuscar.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;
                    string cmd = "Select * FROM Libro WHERE Nombre LIKE ('%" + txtBuscar.Text.Trim() + "%') ";

                    ds = Utilidades.Ejecutar(cmd);
                    dtgvLibros.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error" + error.Message);
                }

            }

        }

        private void Consultar_Libros_FormClosing(object sender, FormClosingEventArgs e)
        {

         
            
        }
    }
}

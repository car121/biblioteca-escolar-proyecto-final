﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Conexion;

namespace Biblioteca_Escolar
{
    public partial class Administrar_Usuario : Form
    {
        public Administrar_Usuario()
        {
            InitializeComponent();
        }

        public Administrar_Usuario(string id)
        {
            InitializeComponent();
            txtIdUsuario.Text = id;

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar_Usuarios ConsultaUsuarios = new Consultar_Usuarios();
            ConsultaUsuarios.ShowDialog();

            if(ConsultaUsuarios.DialogResult== DialogResult.OK)
            {
                txtIdUsuario.Text = ConsultaUsuarios.dtgvLibros.Rows[ConsultaUsuarios.dtgvLibros.CurrentRow.Index].Cells[7].Value.ToString();

                txtNombre.Text= ConsultaUsuarios.dtgvLibros.Rows[ConsultaUsuarios.dtgvLibros.CurrentRow.Index].Cells[0].Value.ToString();

                txtApellidoPaterno.Text= ConsultaUsuarios.dtgvLibros.Rows[ConsultaUsuarios.dtgvLibros.CurrentRow.Index].Cells[1].Value.ToString();

                txtApellidoMaterno.Text= ConsultaUsuarios.dtgvLibros.Rows[ConsultaUsuarios.dtgvLibros.CurrentRow.Index].Cells[2].Value.ToString();

                txtNombreUsuario.Text= ConsultaUsuarios.dtgvLibros.Rows[ConsultaUsuarios.dtgvLibros.CurrentRow.Index].Cells[3].Value.ToString();

                txtContraseña.Text = ConsultaUsuarios.dtgvLibros.Rows[ConsultaUsuarios.dtgvLibros.CurrentRow.Index].Cells[4].Value.ToString();

                txtCantLibros.Text= ConsultaUsuarios.dtgvLibros.Rows[ConsultaUsuarios.dtgvLibros.CurrentRow.Index].Cells[5].Value.ToString();

                cmbAdministrador.Text= ConsultaUsuarios.dtgvLibros.Rows[ConsultaUsuarios.dtgvLibros.CurrentRow.Index].Cells[6].Value.ToString();
            }


        }


       

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            try
            {

                string cmd = string.Format("EXEC ActualizaUsuarios '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'",
                   txtIdUsuario.Text.Trim(),txtNombre.Text.Trim(), txtApellidoMaterno.Text.Trim(), txtApellidoPaterno.Text.Trim(), txtNombreUsuario.Text.Trim(), txtContraseña.Text.Trim(), txtCantLibros.Text.Trim(), cmbAdministrador.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("SE HA GUARDADO CORRECTAMENTE");

                txtIdUsuario.Text = "";
                txtNombre.Text = "";
                txtApellidoMaterno.Text = "";
                txtApellidoPaterno.Text = "";
                txtCantLibros.Text = "";
                txtContraseña.Text = "";
                txtNombreUsuario.Text = "";
                cmbAdministrador.Text = "";
            }
            catch (Exception error)
            {

                MessageBox.Show("Ha Ocurrido un error " + error.Message);

            }
        }

        private void Administrar_Usuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            Ventana_Administrador ventana_Administrador = new Ventana_Administrador();
            ventana_Administrador.Show();
          
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {

                string cmd = string.Format("EXEC EliminaUsuario '{0}'",txtIdUsuario.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("SE HA ELIMINADO CORRECTAMENTE");

                txtIdUsuario.Text = "0";
                txtNombre.Text = "";
                txtApellidoPaterno.Text = "";
                txtApellidoMaterno.Text = "";
                txtCantLibros.Text = "";
                txtContraseña.Text = "";
                txtNombreUsuario.Text = "";
                cmbAdministrador.Text = "";


            }
            catch (Exception error)
            {

                MessageBox.Show("Ha Ocurrido un error " + error.Message);

            }
        }

        private void txtIdUsuario_TextChanged(object sender, EventArgs e)
        {

            

        }


        private void btnNuevo_Click(object sender, EventArgs e)
        {
            txtIdUsuario.Text = "0";
            txtNombre.Text = "";
            txtApellidoPaterno.Text = "";
            txtApellidoMaterno.Text = "";
            txtNombreUsuario.Text = "";
            txtContraseña.Text = "";
            txtCantLibros.Text = "";
            cmbAdministrador.Text = "";
        }

        private void Administrar_Usuario_Load(object sender, EventArgs e)
        {
            txtIdUsuario.Text = "0";
        }
    }
}

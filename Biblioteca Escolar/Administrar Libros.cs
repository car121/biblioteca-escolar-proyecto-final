﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Conexion;

namespace Biblioteca_Escolar
{
    public partial class Administrar_Libros : Form
    {
        public Administrar_Libros()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {

                string cmd = string.Format("EXEC ActualizaLibros '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'",
                   txtIdLibro.Text.Trim(),txtNombre.Text.Trim(),txtAutor.Text.Trim(),txtCategoria.Text.Trim(),txtEditorial.Text.Trim(),txtAño.Text.Trim(),txtExistencia.Text.Trim(),openfileLibro.FileName);
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("SE HA GUARDADO CORRECTAMENTE");


            }
            catch (Exception error)
            {

                MessageBox.Show("Ha Ocurrido un error " + error.Message);

            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar_Libros ConsultaLibros = new Consultar_Libros();
            ConsultaLibros.ShowDialog();


            if (ConsultaLibros.DialogResult == DialogResult.OK)
            {
                txtIdLibro.Text = ConsultaLibros.dtgvLibros.Rows[ConsultaLibros.dtgvLibros.CurrentRow.Index].Cells[7].Value.ToString();

                txtNombre.Text = ConsultaLibros.dtgvLibros.Rows[ConsultaLibros.dtgvLibros.CurrentRow.Index].Cells[0].Value.ToString();

                txtAutor.Text = ConsultaLibros.dtgvLibros.Rows[ConsultaLibros.dtgvLibros.CurrentRow.Index].Cells[1].Value.ToString();

                txtCategoria.Text = ConsultaLibros.dtgvLibros.Rows[ConsultaLibros.dtgvLibros.CurrentRow.Index].Cells[2].Value.ToString();

                txtEditorial.Text = ConsultaLibros.dtgvLibros.Rows[ConsultaLibros.dtgvLibros.CurrentRow.Index].Cells[3].Value.ToString();

                txtAño.Text = ConsultaLibros.dtgvLibros.Rows[ConsultaLibros.dtgvLibros.CurrentRow.Index].Cells[4].Value.ToString();

                txtExistencia.Text = ConsultaLibros.dtgvLibros.Rows[ConsultaLibros.dtgvLibros.CurrentRow.Index].Cells[5].Value.ToString();

                
            }


        }

        private void Administrar_Libros_FormClosing(object sender, FormClosingEventArgs e)
        {
            Ventana_Administrador ventana_Administrador = new Ventana_Administrador();
            ventana_Administrador.Show();

          
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {

                string cmd = string.Format("EXEC EliminarLibros '{0}'",txtIdLibro.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("SE HA ELIMINADO CORRECTAMENTE");

                txtIdLibro.Text = "";

                txtNombre.Text = "";

                txtAutor.Text = "";
                txtAño.Text = "";
                txtCategoria.Text = "";
                txtEditorial.Text = "";
                txtExistencia.Text = "";
                


            }
            catch (Exception error)
            {

                MessageBox.Show("Ha Ocurrido un error " + error.Message);

            }
        }

        private void txtIdLibro_TextChanged(object sender, EventArgs e)
        {

           

        }

        private void txtAutor_TextChanged(object sender, EventArgs e)
        {
     
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
         
           
        }

        private void btnCargarImagen_Click(object sender, EventArgs e)
        {
            if (openfileLibro.ShowDialog() == DialogResult.OK)
            {
                string Imagen = openfileLibro.FileName;

                
            }

            try
            {

   
                MessageBox.Show("SE GUARDO LA IMAGEN");

                pcbLibro.ImageLocation = openfileLibro.FileName;


            }
            catch (Exception error)
            {

                MessageBox.Show("Ha Ocurrido un error " + error.Message);

            }

            

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            txtIdLibro.Text="0";
            txtNombre.Text="";
            txtAutor.Text="";
            txtCategoria.Text="";
            txtEditorial.Text="";
            txtAño.Text="";
            txtExistencia.Text="";
            pcbLibro.ImageLocation = "None";
        }

        private void Administrar_Libros_Load(object sender, EventArgs e)
        {
            txtIdLibro.Text = "0";
        }
    }
}

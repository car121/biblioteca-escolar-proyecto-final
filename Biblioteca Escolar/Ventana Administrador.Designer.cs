﻿namespace Biblioteca_Escolar
{
    partial class Ventana_Administrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // btnAdministrarUsuarios
            // 
            this.btnAdministrarUsuarios.Click += new System.EventHandler(this.btnAdministrarUsuarios_Click);
            // 
            // btnConsultarLibros
            // 
            this.btnConsultarLibros.Click += new System.EventHandler(this.btnConsultarLibros_Click);
            // 
            // btnAdministrarLibros
            // 
            this.btnAdministrarLibros.Click += new System.EventHandler(this.btnAdministrarLibros_Click);
            // 
            // btnConsultarUsuarios
            // 
            this.btnConsultarUsuarios.Click += new System.EventHandler(this.btnConsultarUsuarios_Click);
            // 
            // btnPrestamoLibro
            // 
            this.btnPrestamoLibro.Image = global::Biblioteca_Escolar.Properties.Resources.pdomicilio;
            this.btnPrestamoLibro.Click += new System.EventHandler(this.btnPrestamoLibro_Click);
            // 
            // btnPortadaLibro
            // 
            this.btnPortadaLibro.Click += new System.EventHandler(this.btnPortadaLibro_Click);
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(346, 30);
            this.lblNombre.Size = new System.Drawing.Size(122, 21);
            this.lblNombre.Text = "Administrador";
            // 
            // btnCerrarSesion
            // 
           
            // 
            // Ventana_Administrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(800, 536);
            this.Name = "Ventana_Administrador";
            this.Text = "Ventana_Administrador";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Ventana_Administrador_FormClosing);
            this.Load += new System.EventHandler(this.Ventana_Administrador_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}